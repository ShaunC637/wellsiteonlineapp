angular.module('app.controllers', ['app.services'])
	.factory('settings', ['$rootScope', function($rootScope){
		//supported languages
		
		var settings = {
			languages: [
				{
					language: 'English',
					translation: 'English',
					langCode: 'en',
					flagCode: 'us'
				},
				{
					language: 'Espanish',
					translation: 'Espanish',
					langCode: 'es',
					flagCode: 'es'
				},
				{
					language: 'German',
					translation: 'Deutsch',
					langCode: 'de',
					flagCode: 'de'
				},
				{
					language: 'Korean',
					translation: '한국의',
					langCode: 'ko',
					flagCode: 'kr'
				},
				{
					language: 'French',
					translation: 'français',
					langCode: 'fr',
					flagCode: 'fr'
				},
				{
					language: 'Portuguese',
					translation: 'português',
					langCode: 'pt',
					flagCode: 'br'
				},
				{
					language: 'Russian',
					translation: 'русский',
					langCode: 'ru',
					flagCode: 'ru'
				},
				{
					language: 'Chinese',
					translation: '中國的',
					langCode: 'zh',
					flagCode: 'cn'
				}
			]

		};

		return settings;

	}])

	.controller('PageViewController', ['$scope', '$route', '$animate', function($scope, $route, $animate) {
		// controler of the dynamically loaded views, for DEMO purposes only.
		/*$scope.$on('$viewContentLoaded', function() {

		});*/
	}])

	.controller('SmartAppController', ['$scope', function($scope) {
		// your main controller
	}]);

angular.module('app.demoControllers', [])
    .controller('GraphsController', ['$scope', '$timeout', 'incidentsService', function($scope, $timeout, incidentsService){
        // Load FLOAT dependencies (related to page)
        // setup plot
        var options = {
            grid: {
                hoverable: true,
                clickable: true
            },
            tooltip: true,
            tooltipOpts: {
                defaultTheme: false
            },
            yaxis: {
                min: 0,
                max: 20
            },
            xaxis: {
                mode: "time"
            },
            lines: {
                show: true,
                lineWidth: 1,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.1
                    }, {
                        opacity: 0.13
                    }]
                }
            },
            points: {
                show: true
            }
        };

        $(function () {
          if(!$scope.incidents) {
            $scope.incidents = [];

            incidentsService.getAll().$promise.then(function(allIncidents) {
              $scope.incidents.hseIncidents = {
                  hseNear: $.parseJSON(allIncidents.hse_near),
                  hseReal: $.parseJSON(allIncidents.hse_real)
              };
              $scope.incidents.sqIncidents = {
                  sqNear: $.parseJSON(allIncidents.sq_near),
                  sqReal: $.parseJSON(allIncidents.sq_real)
              };
              loadScript("js/plugin/flot/jquery.flot.cust.min.js", loadFlotResize);

              function loadFlotResize() {
                loadScript("js/plugin/flot/jquery.flot.resize.min.js", loadFlotToolTip);
              }
              function loadFlotToolTip() {
                loadScript("js/plugin/flot/jquery.flot.tooltip.min.js", generatePageGraphs);
              }
            });
          };
        });

        function generatePageGraphs() {

            function getHseChartData() {
              var data = [{
                label: "Real Incident",
                data: $scope.incidents.hseIncidents.hseReal
              }, {
                label: "Near Incident",
                data: $scope.incidents.hseIncidents.hseNear
              }];

              return data;
            }

            function getSqChartData() {
              var data = [{
                label: "Real Incident",  //This is what will show on hover
                data: $scope.incidents.sqIncidents.sqReal
              }, {
                label: "Near Incident",
                data: $scope.incidents.sqIncidents.sqNear
              }];

              return data;
            }

            $('#hse-chart, #sq-chart').bind('plotclick', (function(event, pos, item){
                $scope.incident = [];
                if(item) {
                  $scope.$apply(function () {
                    var incidentDetails = [];
                    for (var i = 0; i < item.series.data[item.dataIndex][2].length; ++i) {
                      incidentDetails.push({Description: item.series.data[item.dataIndex][2][i].Description, ReportedBy: item.series.data[item.dataIndex][2][i].Reported_by, Details: item.series.data[item.dataIndex][2][i].Details});
                    }
                    $scope.incident = incidentDetails;
                  });
                  $('#incidentDetailsModal').modal({show: true});
                }
            }));
            var plot1 = $.plot($("#hse-chart"), getHseChartData(), options);

            var plot2 = $.plot($("#sq-chart"), getSqChartData(), options);
        }
    }])
    .controller('MapsController',  ['$scope', '$window', '$timeout', '$location', 'mapsService', 'latestTruckStatesService', function($scope, $window, $timeout, $location, mapsService, latestTruckStatesService) {
        $scope.lineItemsPerPage = 10;   //For the pagination, this is the number of incidents per page
        var mapData;
        var lineItems = [];
        var markers = [];           //Markers on map

        mapsService.get(function(response) {
          mapData = response;

          // Load Map dependency 1 then call for dependency 2
          loadScript("js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js", loadMapFile);

          // Load Map dependency 2 then rendeder Map
          function loadMapFile() {
            loadScript("js/plugin/vectormap/jquery-jvectormap-us-aea-en.js", renderVectorMap);
          }
        });

        latestTruckStatesService.get(function(response) {
          for (var i = 0; i < response.length; ++i) {
            var t = response[i];
            lineItems.push({truck: t.equipment_number, location: t.location, stagesComplete: t.stages_complete, stagesRemaining: t.stages_remaining, notification: t.notification, id: t.id});
          }
          $scope.lineItems = {};
          $scope.safeApply(lineItemsApply(lineItems));
        });

        function renderVectorMap() {
          $('#vector-map').vectorMap({
            map: 'us_aea_en',
            scaleColors: ['#C8EEFF', '#0071A4'],
            normalizeFunction: 'polynomial',
            focusOn:{
              x: 1.0,
              y: 0.29,
              scale: 3
            },
            series: {
              regions: [{
                scale: {
                  '1': '#3399ff',
                  '2': '#FF69B4'
                },
                attribute: 'fill',
                values: {"US-NY":1, "US-PA":1, "US-WV": 1}
              }]
            },
            hoverOpacity: 0.7,
            hoverColor: false,
            markerStyle: {
              initial: {
                fill: '#F8E23B',
                stroke: '#383f47'
              },
              hover: {
                cursor: 'pointer'
              }
            },
            backgroundColor: '#383f47',
            markers: getMarkers(),
            onMarkerClick: function(r, e){
              $scope.showIncident(markers[e].id);
            }
          });
        }

        function getMarkers() {
            for (var i = 0; i < mapData.length; ++i) {
                var t = mapData[i];
                markers.push({latLng:[parseFloat(t.latitude), parseFloat(t.longitude)], name:t.status, id: t.incidentid});
            }
            return markers;
        }

        function lineItemsApply(lineItems) {
          $scope.lineItems.steps = Math.ceil(lineItems.length/$scope.lineItemsPerPage); //count of how many different pages will be in the pagination
          $scope.lineItems.lastStepStart = Math.floor(lineItems.length/$scope.lineItemsPerPage) * $scope.lineItemsPerPage;
          $scope.lineItems.allLineIncidents = lineItems;
          $scope.lineItems.currentStart = 0;
          $scope.lineItems.lineItem = lineItems.slice(0, $scope.lineItemsPerPage);
        }

        $scope.mapIncidentPagination = function(num) {
            if(num === 0) {
                if($scope.lineItems.lastStepStart == $scope.lineItems.currentStart){
                    return;
                }
                $scope.lineItems.currentStart += $scope.lineItemsPerPage;
            }
            else if (num === -1) {
                if($scope.lineItems.currentStart == 0){
                    return;
                }
                $scope.lineItems.currentStart -= $scope.lineItemsPerPage;
            }
            else {
                $scope.lineItems.currentStart = (num-1) * $scope.lineItemsPerPage;
            }
            $scope.lineItems.lineIncidents = $scope.lineItems.allLineIncidents.slice($scope.lineItems.currentStart, $scope.lineItems.currentStart + $scope.lineItemsPerPage);
        }

        $scope.getNumber = function(num) {
            return new Array(num);
        };

        $scope.safeApply = function(fn) {
          var phase = this.$root.$$phase;
          if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
              fn();
            }
          } else {
            this.$apply(fn);
          }
        };

        $scope.showIncident = function(id) {
          $window.sessionStorage.incidentId = id;
          //$location.path('/forms/form-elements');
          $scope.safeApply($location.path('/incidents/incident'));
        };
    }])
  .controller('IncidentController',  ['$scope', '$window', 'incidentsService', function($scope, $window, incidentsService) {
    if($window.sessionStorage.incidentId) {
      $('.jvectormap-label').remove();
      incidentsService.getSingle({id: $window.sessionStorage.incidentId}, function (incident) {
        $scope.incident = {};
        $scope.incident.id = $window.sessionStorage.incidentId;
        $scope.incident.description = {};
        $scope.incident.reporter = incident.reporter;
        $scope.incident.incident_date = incident.incident_date;
        $scope.incident.incident_time = incident.incident_time;
        $scope.incident.well_name = incident.well_name;
        $scope.incident.client = incident.client;
        $scope.incident.truck_number = incident.truck_number;
        $scope.incident.type = incident.type;
        if (incident.decription) {
          $scope.incident.description = {
            operations_involved: incident.decription.operations_involved,
            what_happened: incident.decription.what_happened,
            result: incident.decription.result,
            cause: incident.decription.cause
          };
        }
        $scope.incident.action_taken = incident.action_taken;
        $('#spinEdit').spinedit();
      });
    }
    $window.sessionStorage.incidentId = null;
  }])
;
