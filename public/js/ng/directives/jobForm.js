/**
 * Created by shaunconnolly on 11/1/14.
 */
smartApp
  .directive('jobForm', function() {
    return {
      restrict: 'E',
      replace:true,
      templateUrl: "templates/jobTemplate.html"
      }
    }
 )