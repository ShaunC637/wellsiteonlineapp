/**
 * Created by shaunconnolly on 11/2/14.
 */
smartApp
  .directive('numberSpinner', function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        element.spinedit();
      }
    }
  }
)