/**
 * Created by shaunconnolly on 11/2/14.
 */
smartApp
  .directive('incidentForm', function() {
    return {
      restrict: 'E',
      replace:true,
      templateUrl: "templates/incidentTemplate.html"
    }
  }
)