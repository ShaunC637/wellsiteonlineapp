/**
 * Created by shaunconnolly on 10/8/14.
 */
angular.module('app.services', ['ngResource'])
    .factory('incidentsService', ['$resource', function($resource) {
        return $resource('http://wellsitereport-env.elasticbeanstalk.com/incidents', {},
            {
                getAll: {
                    method: 'GET',
                    url: 'http://wellsitereport-env.elasticbeanstalk.com/incidents'
                },
                getSingle: {
                    method: 'GET',
                    url: 'http://wellsitereport-env.elasticbeanstalk.com/incidents/:id',
                    params: { id: '@id'}
                }
            });
    }])
    .factory('mapsService', ['$resource', function($resource) {
        return $resource('http://wellsitereport-env.elasticbeanstalk.com/incidents/maps', {},
            {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
    }])
    .factory('latestTruckStatesService', ['$resource', function($resource) {
      return $resource('http://wellsitereport-env.elasticbeanstalk.com/latest_truck_states', {},
        {
          get: {
            method: 'GET',
            isArray: true
          }
        });
    }]);